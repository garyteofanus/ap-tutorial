package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {

    @Override
    public String getType() {
        return "Attack With Gun";
    }

    @Override
    public String attack() {
        return "You've met the fastest gun in the west";
    }
}
