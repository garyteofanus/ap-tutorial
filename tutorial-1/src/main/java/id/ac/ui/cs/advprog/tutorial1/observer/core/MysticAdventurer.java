package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {


    public MysticAdventurer(Guild guild) {
        this.name = "Mystic";
        this.guild = guild;
    }

    @Override
    public void update() {
        // Add quest if it's escort or delivery type
        if (guild.getQuestType().equals("E") || guild.getQuestType().equals("D")) {
            getQuests().add(guild.getQuest());
        }
    }
}
