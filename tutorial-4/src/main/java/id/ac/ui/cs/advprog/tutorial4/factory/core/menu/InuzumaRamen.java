package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.factory.InuzumaRamenIngredientsFactory;

public class InuzumaRamen extends Menu {
    //Ingridients:
    //Noodle: Ramen
    //Meat: Pork
    //Topping: Boiled Egg
    //Flavor: Spicy
    public InuzumaRamen(String name){
        super(name, new InuzumaRamenIngredientsFactory());
    }
}
