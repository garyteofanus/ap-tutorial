package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MondoUdonTest {
    private MondoUdon menu;

    @BeforeEach
    public void setup() {
        menu = new MondoUdon("Mondo Udon");
    }

    @Test
    public void testGetName() {
        assertEquals("Mondo Udon", menu.getName());
    }

    @Test
    public void testGetNoodle() {
        assertEquals(Udon.class, menu.getNoodle().getClass());
    }

    @Test
    public void testGetMeat() {
        assertEquals(Chicken.class, menu.getMeat().getClass());
    }

    @Test
    public void testGetTopping() {
        assertEquals(Cheese.class, menu.getTopping().getClass());
    }

    @Test
    public void testGetFlavor() {
        assertEquals(Salty.class, menu.getFlavor().getClass());
    }
}
