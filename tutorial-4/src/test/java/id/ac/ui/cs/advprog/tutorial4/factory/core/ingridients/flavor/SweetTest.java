package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SweetTest {
    @Test
    public void testSweetFlavorDescription() {
        Sweet sweet = new Sweet();
        assertEquals("Adding a dash of Sweet Soy Sauce...", sweet.getDescription());
    }
}
