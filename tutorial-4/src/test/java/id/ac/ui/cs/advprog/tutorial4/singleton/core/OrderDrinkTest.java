package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OrderDrinkTest {
    @Test
    public void testNoDuplicateOrderDrinkInstance() {
        OrderDrink instance1 = OrderDrink.getInstance();
        OrderDrink instance2 = OrderDrink.getInstance();
        assertEquals(instance1, instance2);
    }

    @Test
    public void testDrinkGetterSetter() {
        OrderDrink.getInstance().setDrink("testDrink");
        assertEquals("testDrink", OrderDrink.getInstance().getDrink());
    }

    @Test
    public void testToStringMethodReturnDrink() {
        OrderDrink.getInstance().setDrink("testDrink");
        assertEquals("testDrink", OrderDrink.getInstance().toString());
    }

    @Test
    public void testToStringMethodWhenNoDrinkIsSet() {
        assertEquals("No Order yet. Order first.", OrderDrink.getInstance().toString());
    }
}
