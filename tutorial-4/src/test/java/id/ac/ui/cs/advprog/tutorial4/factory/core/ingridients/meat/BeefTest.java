package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BeefTest {
    @Test
    public void testBeefMeatDescription() {
        Beef beef = new Beef();
        assertEquals("Adding Maro Beef Meat...", beef.getDescription());
    }
}