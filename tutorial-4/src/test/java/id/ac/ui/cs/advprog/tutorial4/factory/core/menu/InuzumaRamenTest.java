package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class InuzumaRamenTest {
    private InuzumaRamen menu;

    @BeforeEach
    public void setup() {
        menu = new InuzumaRamen("Inuzuma Ramen");
    }

    @Test
    public void testGetName() {
        assertEquals("Inuzuma Ramen", menu.getName());
    }

    @Test
    public void testGetNoodle() {
        assertEquals(Ramen.class, menu.getNoodle().getClass());
    }

    @Test
    public void testGetMeat() {
        assertEquals(Pork.class, menu.getMeat().getClass());
    }

    @Test
    public void testGetTopping() {
        assertEquals(BoiledEgg.class, menu.getTopping().getClass());
    }

    @Test
    public void testGetFlavor() {
        assertEquals(Spicy.class, menu.getFlavor().getClass());
    }
}
