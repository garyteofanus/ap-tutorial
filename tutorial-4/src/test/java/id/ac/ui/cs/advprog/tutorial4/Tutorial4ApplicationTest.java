package id.ac.ui.cs.advprog.tutorial4;

import org.junit.jupiter.api.Test;

public class Tutorial4ApplicationTest {
    // Only to invoke Tutorial4Application main function
    @Test
    public void testMainApplication() {
        Tutorial4Application.main(new String[] {});
    }
}
