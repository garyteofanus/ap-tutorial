package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class IngredientsFactoryTest {
    private Class<?> ingridientsFactoryClass;

    @BeforeEach
    public void setup() throws Exception{
        ingridientsFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.factory.IngredientsFactory");
    }

    @Test
    public void testIngredientsFactoryIsAPublicInterface() {
        int classModifiers = ingridientsFactoryClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testIngredientsFactoryHasAbstractCreateNoodleMethod() throws Exception {
        Method getDescription = ingridientsFactoryClass.getDeclaredMethod("createNoodle");
        int methodModifiers = getDescription.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getDescription.getParameterCount());
    }

    @Test
    public void testIngredientsFactoryHasAbstractCreateFlavorMethod() throws Exception {
        Method getDescription = ingridientsFactoryClass.getDeclaredMethod("createFlavor");
        int methodModifiers = getDescription.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getDescription.getParameterCount());
    }

    @Test
    public void testIngredientsFactoryHasAbstractCreateMeatMethod() throws Exception {
        Method getDescription = ingridientsFactoryClass.getDeclaredMethod("createMeat");
        int methodModifiers = getDescription.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getDescription.getParameterCount());
    }

    @Test
    public void testIngredientsFactoryHasAbstractCreateToppingMethod() throws Exception {
        Method getDescription = ingridientsFactoryClass.getDeclaredMethod("createTopping");
        int methodModifiers = getDescription.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getDescription.getParameterCount());
    }
}
