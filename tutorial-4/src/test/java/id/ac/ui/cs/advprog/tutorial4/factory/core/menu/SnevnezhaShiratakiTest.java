package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SnevnezhaShiratakiTest {
    private SnevnezhaShirataki menu;

    @BeforeEach
    public void setup() {
        menu = new SnevnezhaShirataki("Snevnezha Shirataki");
    }

    @Test
    public void testGetName() {
        assertEquals("Snevnezha Shirataki", menu.getName());
    }

    @Test
    public void testGetNoodle() {
        assertEquals(Shirataki.class, menu.getNoodle().getClass());
    }

    @Test
    public void testGetMeat() {
        assertEquals(Fish.class, menu.getMeat().getClass());
    }

    @Test
    public void testGetTopping() {
        assertEquals(Flower.class, menu.getTopping().getClass());
    }

    @Test
    public void testGetFlavor() {
        assertEquals(Umami.class, menu.getFlavor().getClass());
    }
}
