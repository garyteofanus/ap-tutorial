package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.factory.factory.SnevnezhaShiratakiIngredientsFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SnevnezhaShiratakiIngredientsFactoryTest {
    SnevnezhaShiratakiIngredientsFactory factory;

    @BeforeEach
    public void setup() {
        factory = new SnevnezhaShiratakiIngredientsFactory();
    }

    @Test
    public void testCreateNoodleShouldReturnRamen() {
        assertEquals(Shirataki.class, factory.createNoodle().getClass());
    }

    @Test
    public void testCreateMeatShouldReturnPork() {
        assertEquals(Fish.class, factory.createMeat().getClass());
    }

    @Test
    public void testCreateToppingShouldReturnBoiledEgg() {
        assertEquals(Flower.class, factory.createTopping().getClass());
    }

    @Test
    public void testCreateFlavorShouldReturnSpicy() {
        assertEquals(Umami.class, factory.createFlavor().getClass());
    }
}
