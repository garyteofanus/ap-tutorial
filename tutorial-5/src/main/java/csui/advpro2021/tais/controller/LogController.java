package csui.advpro2021.tais.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.service.LogService;
import csui.advpro2021.tais.service.MahasiswaService;

@RestController
@RequestMapping(path = "/log")
public class LogController {
    @Autowired
    private LogService logService;

    @Autowired
    private MahasiswaService mahasiswaService;

    @GetMapping(produces = { "application/json" })
    @ResponseBody
    public ResponseEntity getAllLogs() {
        return ResponseEntity.ok(logService.getAllLogs());
    }

    @PostMapping(path = "/{npm}", produces = { "application/json" })
    @ResponseBody
    public ResponseEntity createLog(@PathVariable(value = "npm") String npm, @RequestBody Log log) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        if (mahasiswa == null)
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        if (mahasiswa.getMatkulAsdos() == null)
            return new ResponseEntity(HttpStatus.NOT_IMPLEMENTED);

        return ResponseEntity.ok(logService.createLog(mahasiswa, log));
    }

    @GetMapping(path = "/{npm}", produces = { "application/json" })
    @ResponseBody
    public ResponseEntity getLogByNPM(@PathVariable(value = "npm") String npm) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        if (mahasiswa == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        if (mahasiswa.getMatkulAsdos() == null) {
            return new ResponseEntity(HttpStatus.NOT_IMPLEMENTED);
        }

        return ResponseEntity.ok(mahasiswa.getLogList());
    }

    @PutMapping(path = "/{idLog}", produces = { "application/json" })
    @ResponseBody
    public ResponseEntity updateLog(@PathVariable(value = "idLog") Integer idLog, @RequestBody Log log) {
        return ResponseEntity.ok(logService.updateLog(idLog, log));
    }

    @DeleteMapping(path = "/{idLog}", produces = { "application/json" })
    public ResponseEntity deleteLog(@PathVariable(value = "idLog") Integer idLog) {
        Log log = logService.getLogByIdLog(idLog);
        if (log == null)
            return new ResponseEntity(HttpStatus.NOT_FOUND);

        logService.deleteLogByIdLog(idLog);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @GetMapping(path = "/{npm}/summary", produces = { "application/json" })
    @ResponseBody
    public ResponseEntity getLogSummary(@PathVariable(value = "npm") String npm) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        return ResponseEntity.ok(logService.getLogSummary(mahasiswa));
    }

    @GetMapping(path = "/{npm}/summary/{month}", produces = { "application/json" })
    @ResponseBody
    public ResponseEntity getLogSummaryByMonth(@PathVariable(value = "npm") String npm, @PathVariable(value = "month") int month) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        if (mahasiswa == null) return new ResponseEntity(HttpStatus.NOT_FOUND);
        if (mahasiswa.getMatkulAsdos() == null) return new ResponseEntity(HttpStatus.NOT_IMPLEMENTED);
        return ResponseEntity.ok(logService.getLogSummaryMonth(mahasiswa, month - 1));
    }

}
