package csui.advpro2021.tais.service;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;

@Service
public class LogServiceImpl implements LogService {
    @Autowired
    private LogRepository logRepository;

    @Autowired
    private MahasiswaService mahasiswaService;

    @Override
    public Log createLog(Mahasiswa mahasiswa, Log log) {
        log.setMahasiswa(mahasiswa);
        logRepository.save(log);
//        mahasiswa.getLogList().add(log);
//        mahasiswaService.updateMahasiswa(mahasiswa.getNpm(), mahasiswa);
        return log;
    }

    @Override
    public Log getLogByIdLog(Integer idLog) {
        return logRepository.findByIdLog(idLog);
    }

    @Override
    public Log updateLog(Integer idLog, Log log) {
        log.setIdLog(idLog);
        logRepository.save(log);
        return log;
    }

    @Override
    public void deleteLogByIdLog(Integer idLog) {
        logRepository.deleteById(idLog);
    }

    @Override
    public List<Log> getAllLogsByMahasiswa(Mahasiswa mahasiswa) {
        return logRepository.findAllLogByMahasiswa(mahasiswa);
    }

    @Override
    public Collection<LogSummary> getLogSummary(Mahasiswa mahasiswa) {
        Calendar calendar = Calendar.getInstance();

        List<Log> logList = getAllLogsByMahasiswa(mahasiswa);
        Map<Integer, LogSummary> result = new TreeMap<>();
        for (Log log : logList) {
            Date start = log.getStartTime();
            Date end = log.getEndTime();
            int diff = (int) ((end.getTime() - start.getTime()) /3600000);

            calendar.setTime(start);
            int month = calendar.get(Calendar.MONTH);
            String monthStr = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());

            LogSummary logSummary = result.getOrDefault(month, new LogSummary(monthStr));
            logSummary.setJamKerja(logSummary.getJamKerja() + diff);
            logSummary.setPembayaran(logSummary.getJamKerja() * 350);

            result.put(month, logSummary);
        }

        return result.values();
    }

    @Override
    public LogSummary getLogSummaryMonth(Mahasiswa mahasiswa, int month) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(0, month, 1);

        List<Log> logList = getAllLogsByMahasiswa(mahasiswa);

        String monthStr = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
        LogSummary logSummary = new LogSummary(monthStr);
        for (Log log : logList) {
            calendar.setTime(log.getStartTime());
            if (calendar.get(Calendar.MONTH) == month) {
                Date start = log.getStartTime();
                Date end = log.getEndTime();
                int difference = (int) ((end.getTime() - start.getTime()) / 3600000);

                logSummary.setJamKerja(logSummary.getJamKerja() + difference);
            }
        }
        logSummary.setPembayaran(logSummary.getJamKerja() * 350);

        return logSummary;

    }

    @Override
    public List<Log> getAllLogs() {
        return logRepository.findAll();
    }
    
}
