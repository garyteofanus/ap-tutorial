package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class LogServiceImplTest {
    @Mock
    private LogRepository logRepository;

    @InjectMocks
    private LogServiceImpl logService;

    private Log log;

    private Mahasiswa mahasiswa;

    @BeforeEach
    public void setUp() {
        mahasiswa = new Mahasiswa("1906192052", "Maung Meong", "maung@cs.ui.ac.id", "4",
                "081317691718");

        Calendar calendar = Calendar.getInstance();
        calendar.set(2000, 1, 1, 0, 0, 0);
        Date start = calendar.getTime();
        calendar.set(2000, 1, 1, 4, 0, 0);
        Date end = calendar.getTime();

        log = new Log(start, end, "Sebuah Deskripsi");
        log.setIdLog(1);
        log.setMahasiswa(mahasiswa);
    }

    @Test
    public void testServiceCreateLog() {
        lenient().when(logService.createLog(mahasiswa, log)).thenReturn(log);
    }

    @Test
    public void testServiceGetLogByIdLog() {
        lenient().when(logService.getLogByIdLog(1)).thenReturn(log);
        Log resultLog = logService.getLogByIdLog(log.getIdLog());
        assertEquals(log.getIdLog(), resultLog.getIdLog());
    }

    @Test
    public void testLogDeleteLog() {
        logService.createLog(mahasiswa, log);
        logService.deleteLogByIdLog(log.getIdLog());
        lenient().when(logService.getLogByIdLog(log.getIdLog())).thenReturn(null);
        assertEquals(null, logService.getLogByIdLog(log.getIdLog()));
    }

    @Test
    public void testServiceUpdateLog() {
        logService.createLog(mahasiswa, log);
        String currentDesc = log.getDeskripsi();
        log.setDeskripsi("AAAA");

        lenient().when(logService.updateLog(log.getIdLog(), log)).thenReturn(log);
        Log resultLog = logService.updateLog(log.getIdLog(), log);

        assertNotEquals(resultLog.getDeskripsi(), currentDesc);
        assertEquals(resultLog.getEndTime(), log.getEndTime());
    }

    @Test
    public void testServiceGetSummary() {
        List<Log> logList = new ArrayList<>();
        logList.add(log);
        lenient().when(logRepository.findAllLogByMahasiswa(mahasiswa)).thenReturn(logList);

        List<LogSummary> expectedLogList = new ArrayList<>();
        expectedLogList.add(new LogSummary("February", 4, 1400));

        Collection<LogSummary> logSummaries = logService.getLogSummary(mahasiswa);
        assertIterableEquals(expectedLogList, logSummaries);
    }

    @Test
    public void testServiceGetSummaryMonth() {
        List<Log> logList = new ArrayList<>();
        logList.add(log);
        lenient().when(logRepository.findAllLogByMahasiswa(mahasiswa)).thenReturn(logList);

        LogSummary expectedSummary = new LogSummary("February", 4, 1400);

        LogSummary logSummaries = logService.getLogSummaryMonth(mahasiswa, 1);
        assertEquals(expectedSummary, logSummaries);
    }

    @Test
    public void testGetAllLogs() {
        List<Log> listLog = logRepository.findAll();
        lenient().when(logService.getAllLogs()).thenReturn(listLog);
        List<Log> listLogResult = logService.getAllLogs();
        assertEquals(listLog, listLogResult);
    }
}
