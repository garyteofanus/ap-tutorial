## Mahasiswa

* Membuat Instance Mahasiswa
* Mengembalikan semua instance mahasiswa
* Update data mahasiswa
* Menghapus Mahasiswa
* Mengembalikan sebuah instance mahasiswa
* Mendaftar mahasiswa sebagai seorang asdos

## Mata Kuliah

* Mendaftar semua mata kuliah
* Membuat Instance mata kuliah
* Mengecek sebuah Instance mata kuliah
* Mengambil instance mata kuliah dengan kode tertentu
* Update informasi mata kuliah

## Log

* Mengembalikan semua log seorang mahasiswa berdasarkan NPM
* Menambahkan log
* Update log
* Hapus log
* Mendapatkan summary log untuk semua bulan
* Mendapatkan summary log pada bulan tertentu
