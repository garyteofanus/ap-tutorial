package id.ac.ui.cs.advprog.tutorial3.facade.core.misc;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TranslatorTest {
    private Class<?> translatorClass;

    @BeforeEach
    public void setup() throws Exception {
        translatorClass = Translator.class;
    }

    @Test
    public void testTranslatorClassIsAPublicClass() {
        int classModifiers = translatorClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
    }

    @Test
    public void testTranslatorClassHasEncodeMethod() throws Exception {
        Method translate = translatorClass.getDeclaredMethod("encode", String.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testTranslatorClassHasDecodeMethod() throws Exception {
        Method translate = translatorClass.getDeclaredMethod("decode", String.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testTranslatorEncodeMethodEncodesCorrectly() throws Exception{
        Translator mockTranslator = new Translator();
        assertEquals("rJdCJZ_zJZ_}xBJZn", mockTranslator.encode("Tampan dan Berani"));
    }

    @Test
    public void testTranslatorDecodeMethodDecodesCorrectly() throws Exception {
        Translator mockTranslator = new Translator();
        assertEquals("Tampan dan Berani", mockTranslator.decode("rJdCJZ_zJZ_}xBJZn"));
    }

}
