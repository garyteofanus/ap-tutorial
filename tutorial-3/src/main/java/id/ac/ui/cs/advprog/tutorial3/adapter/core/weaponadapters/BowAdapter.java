package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;

public class BowAdapter implements Weapon {

    private Bow bow;
    // Make an attackMode field to keep track of current attack mode that can
    // influence normalAttack and chargedAttack behaviour
    private Boolean attackMode;

    public BowAdapter(Bow bow) {
        this.bow = bow;
        this.attackMode = false;
    }

    // Normal attack calls shoot arrow method on bow according to attackMode
    @Override
    public String normalAttack() {
        return bow.shootArrow(this.attackMode);
    }

    // Charged attack change attackMode
    @Override
    public String chargedAttack() {
        this.attackMode = !this.attackMode;

        if (attackMode) {
            return "Entering aim shot mode";
        } else {
            return "Leaving aim shot mode";
        }
    }

    @Override
    public String getName() {
        return bow.getName();
    }

    @Override
    public String getHolderName() {
        return bow.getHolderName();
    }
}
